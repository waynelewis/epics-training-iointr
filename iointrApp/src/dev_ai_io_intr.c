#include <devSup.h>
#include <dbScan.h>
#include <aiRecord.h>
#include <epicsExport.h>
#include <epicsThread.h>

static IOSCANPVT ioscanpvt;
static epicsThreadId worker_thread_id;
int worker_thread(void *priv);

// Record initialisation function. The thread for updating the record value is
// created here. Normally the thread would be created independently of record
// initialization, as multiple record may depend on data from thread. For this
// simple example, it is sufficient to create the thread here.

static long initRecord(struct aiRecord *prec){

    worker_thread_id = epicsThreadCreate("aiIOIntrThread",
            epicsThreadPriorityMedium,
            epicsThreadGetStackSize(epicsThreadStackMedium),
            (EPICSTHREADFUNC)worker_thread,
            NULL);

    prec->rval = 0;
    prec->dpvt = 0;
    return 0;
}

// Increment the record value each time it processes.

static long read(struct aiRecord *prec)
{
    prec->rval = prec->rval + 1;
    return 0;
}

// Setup for the I/O Intr processing. Puts this record into the ioscanpvt list
// for processing.

static long get_ioint_info(
        int   cmd,
        struct aiRecord   *pr,
        IOSCANPVT   *ppvt)
{
    *ppvt = ioscanpvt;
    return(0);
}

// Create the device support entry table.

static struct {
    long number;
    long (*report)(int);
    long (*initialize)(int);
    long (*initRecord)(struct aiRecord *);
    long (*getIoIntInfo)(int, struct aiRecord *, IOSCANPVT *);
    long (*read)(struct aiRecord *);
    long (*linConv)(struct aiRecord *);
} devAiIOIntr = {
    6, NULL, NULL, initRecord, get_ioint_info, read, NULL
};

// Thread for triggering record processing. Simple sleep function triggers the
// record periodically.

int worker_thread(void *priv) {
    scanIoInit(&ioscanpvt);
    while (1) {
        scanIoRequest(ioscanpvt);
        epicsThreadSleep(2.0);
    }
}

epicsExportAddress(dset,devAiIOIntr);

