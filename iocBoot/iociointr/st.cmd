#!../../bin/darwin-x86/iointr

## You may have to change iointr to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/iointr.dbd"
iointr_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/ai_io_intr.db","")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=wayne"
