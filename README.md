# EPICS I/O Interrupt processing demonstration

This IOC is intended to be a minimal working example of I/O Intr record processing in an EPICS IOC.

This IOC uses the standard EPICS build system.

It creates a single PV that increments every two seconds when triggered by the worker thread.
